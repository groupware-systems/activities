<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2019 Stephan Kreutzer

This file is part of Activities.

Activities is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

Activities is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with Activities. If not, see <http://www.gnu.org/licenses/>.
-->
<!-- Works on output of https://github.com/Spoowy/org-to-xml -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="htx-scheme-id://org.groupware-systems.20190227T133048Z/activities.20190512T211200Z" xmlns:src="https://nwalsh.com/ns/org-to-xml" exclude-result-prefixes="src">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no"/>

  <xsl:template match="/src:org-data">
    <activities xml:lang="en">
      <xsl:comment> This file was created by orgxml_to_activities_xml_1.xsl of Activities, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/groupware-systems/activities/ and http://www.groupware-systems.org). </xsl:comment>
      <xsl:apply-templates select="./src:headline"/>
    </activities>
  </xsl:template>

  <!-- TODO: [...] in @closed are markup, not data. -->

  <xsl:template match="src:headline">
    <activity>
      <xsl:choose>
        <xsl:when test="@todo-type='todo'">
          <xsl:attribute name="status">
            <xsl:text>filed</xsl:text>
          </xsl:attribute>
        </xsl:when>
        <xsl:when test="@todo-type='done'">
          <xsl:attribute name="status">
            <xsl:text>complete</xsl:text>
          </xsl:attribute>
          <xsl:if test="@closed">
            <xsl:attribute name="completed">
              <xsl:value-of select="@closed"/>
            </xsl:attribute>
          </xsl:if>
        </xsl:when>
      </xsl:choose>
      <title>
        <xsl:apply-templates select="./src:title"/>
      </title>
      <xsl:if test="./src:section/src:paragraph">
        <description>
          <xsl:apply-templates select="./src:section/src:paragraph"/>
        </description>
      </xsl:if>
      <xsl:if test="./src:headline">
        <sub-activities>
          <xsl:apply-templates select="./src:headline"/>
        </sub-activities>
      </xsl:if>
    </activity>
  </xsl:template>

  <xsl:template match="src:headline/src:title//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="src:headline/src:title//src:link">
    <a href="{@raw-link}">
      <xsl:apply-templates/>
    </a>
  </xsl:template>

  <xsl:template match="src:headline/src:section/src:paragraph//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="src:headline/src:section/src:paragraph//src:link">
    <a href="{@raw-link}">
      <xsl:apply-templates/>
    </a>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>

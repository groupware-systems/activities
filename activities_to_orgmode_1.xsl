<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2019  Stephan Kreutzer

This file is part of Activities.

Activities is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

Activities is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with Activities. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:src="htx-scheme-id://org.groupware-systems.20190227T133048Z/activities.20190512T211200Z">
  <!-- TODO: Beware of Org-Mode special characters! No escaping yet! -->
  <xsl:output method="text" encoding="UTF-8"/>

  <xsl:template match="/">
    <xsl:apply-templates select="./src:activities/src:activity"/>
  </xsl:template>

  <xsl:template match="src:activity">
    <xsl:for-each select="ancestor-or-self::src:activity">
      <xsl:text>*</xsl:text>
    </xsl:for-each>
    <xsl:choose>
      <xsl:when test="@status='complete'">
        <xsl:text> DONE </xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text> TODO </xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:apply-templates select="./src:title"/>
    <xsl:if test="@status='complete' and @completed">
      <xsl:text>&#xA;</xsl:text>
      <xsl:for-each select="ancestor-or-self::src:activity">
        <xsl:text> </xsl:text>
      </xsl:for-each>
      <xsl:text> CLOSED: [</xsl:text>
        <xsl:value-of select="@completed"/>
      <xsl:text>]</xsl:text>
    </xsl:if>
    <xsl:if test="./src:description">
      <xsl:text>&#xA;</xsl:text>
      <xsl:for-each select="ancestor-or-self::src:activity">
        <xsl:text> </xsl:text>
      </xsl:for-each>
      <xsl:text> </xsl:text>
      <xsl:apply-templates select="./src:description"/>
    </xsl:if>
    <xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates select="./src:sub-activities/src:activity"/>
  </xsl:template>

  <xsl:template match="src:activity/src:title">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="src:activity/src:title//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="src:activity/src:title//src:a">
    <xsl:text>[[</xsl:text>
    <xsl:value-of select="@href"/>
    <xsl:text>][</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>]]</xsl:text>
  </xsl:template>

  <xsl:template match="src:activity/src:title//src:a//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="src:activity/src:description">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="src:activity/src:description//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="src:activity/src:description//src:a">
    <xsl:text>[[</xsl:text>
    <xsl:value-of select="@href"/>
    <xsl:text>][</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>]]</xsl:text>
  </xsl:template>

  <xsl:template match="src:activity/src:description//src:a//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>

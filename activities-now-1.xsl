<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2019 Stephan Kreutzer

This file is part of Activities.

Activities is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

Activities is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with Activities. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:src="htx-scheme-id://org.groupware-systems.20190227T133048Z/activities.20190512T211200Z" exclude-result-prefixes="src">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:comment> This file was created by activities-now-1.xsl of Activities, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/groupware-systems/activities/ and http://www.groupware-systems.org). </xsl:comment>
<xsl:text>&#x0A;</xsl:text>
        <title>Now</title>
        <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport"/>
        <style type="text/css">
          body
          {
              font-family: sans-serif;
          }

          .active
          {
              color: #FFA500;
          }

          .complete
          {
              color: #008000;
              text-decoration: line-through;
          }
        </style>
      </head>
      <body>
        <div>
          <h1>Now</h1>
          <xsl:if test="./src:activities/src:activity[@status='active']">
            <ul>
              <xsl:apply-templates select="./src:activities/src:activity[@status='active']"/>
            </ul>
          </xsl:if>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="src:activity[@status='active']">
    <li>
      <span class="active">
        <xsl:apply-templates select="./src:title"/>
      </span>
      <xsl:if test="./src:sub-activities/src:activity[@status='active']">
        <ul>
          <xsl:apply-templates select="./src:sub-activities/src:activity[@status='active']"/>
        </ul>
      </xsl:if>
    </li>
  </xsl:template>

  <xsl:template match="//src:activity/src:title//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="//src:activity/src:title//src:a">
    <a href="{@href}"><xsl:apply-templates/></a>
  </xsl:template>

  <xsl:template match="//src:activity/src:title//src:a//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="//src:activity/src:description//src:a">
    <a href="{@href}"><xsl:apply-templates/></a>
  </xsl:template>

  <xsl:template match="//src:activity/src:description//src:a//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>

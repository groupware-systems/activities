<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2018-2023 Stephan Kreutzer

This file is part of Activities.

Activities is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

Activities is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with Activities. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:src="htx-scheme-id://org.groupware-systems.20190227T133048Z/activities.20190512T211200Z" exclude-result-prefixes="src">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="de" lang="de">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:comment> This file was created by activities-xhtml-1_de.xsl of Activities, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/groupware-systems/activities/ and http://www.groupware-systems.org). </xsl:comment>
<xsl:text>&#x0A;</xsl:text>
<xsl:comment>
Copyright (C) 2018-2019 Stephan Kreutzer

This program is part of Activities.

Activities is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

Activities is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with Activities. If not, see &lt;http://www.gnu.org/licenses/&gt;.

The textual content in the &lt;div id="content"/&gt; is not part of this
program, it's user data that is only processed. A different license may apply.
</xsl:comment>
<xsl:text>&#x0A;</xsl:text>
        <title>Aktivitäten</title>
        <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport"/>
        <style type="text/css">
          body
          {
              font-family: sans-serif;
          }

          .active
          {
              color: #FFA500;
          }

          .complete
          {
              color: #008000;
              text-decoration: line-through;
          }

          .abandoned
          {
              text-decoration: line-through;
          }

          #content-pane
          {
              float: left;
              width: 50%;
              position: relative;
          }

          #description-pane
          {
              right: 0;
              width: 50%;
              border-left: 2px solid black;
              box-sizing: border-box;
              position: fixed;
          }

          /*
          #content-target
          {
              padding: 2em;
              text-align: justify;
          }
          */

          #description-target
          {
               padding: 2em;
              text-align: justify;
          }

          #descriptions
          {
              display: none;
          }
        </style>
        <script type="text/javascript">
          "use strict";

          function showDescription(id)
          {
              hideDescription();

              let description = document.getElementsByClassName(id);

              if (description == null)
              {
                  return -1;
              }

              if (description.length &lt;= 0)
              {
                  return -1;
              }

              let parent = document.getElementById('description-target');

              if (parent == null)
              {
                  return -1;
              }

              let destination = document.createElement("div");
              parent.appendChild(destination);

              for (let i = 0; i &lt; description.length; i++)
              {
                  let element = description[i].cloneNode(true);
                  // Otherwise DOM would update description.length as the nodes would
                  // be cloned with their class attribute, leading to an infinite loop.
                  element.removeAttribute("class");
                  destination.appendChild(element);
              }

              return 0;
          }

          function hideDescription()
          {
              let parent = document.getElementById('description-target');

              if (parent == null)
              {
                  return -1;
              }

              let description = parent.getElementsByTagName("div");

              // Stupid JavaScript has a cloneNode(deep), but no removeNode(deep).
              let removeNode = function(element, parent)
              {
                  while (element.hasChildNodes() == true)
                  {
                      removeNode(element.lastChild, element);
                  }

                  parent.removeChild(element);
              }

              for (let i = 0; i &lt; description.length; i++)
              {
                  removeNode(description[i], parent);
              }

              return 0;
          }
        </script>
      </head>
      <body>
        <div id="content">
          <xsl:comment> The textual content in this element and sub-elements is not part of this program, it's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. </xsl:comment>
          <div id="content-pane">
            <h2>Aktivitäten</h2>
            <div id="content-target">
              <xsl:if test="./src:activities/src:activity[not(@status='complete')]">
                <ul>
                  <xsl:apply-templates select="./src:activities/src:activity[not(@status='complete')]"/>
                </ul>
              </xsl:if>
            </div>
          </div>
          <div id="description-pane">
            <h2>Beschreibung Aktivität</h2>
            <div id="description-target">
              <div>
                <p>
                  Auf einen am Ende mit Stern (*) gekennzeichneten Aktivitäts-Eintrag klicken, um dessen Beschreibung anzuzeigen.
                </p>
              </div>
            </div>
          </div>
          <div id="descriptions" class="descriptions">
            <h2>Beschreibungen</h2>
            <xsl:apply-templates select=".//src:description"/>
          </div>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="src:activity">
    <li>
      <span>
        <xsl:attribute name="onclick">
          <xsl:text>showDescription('term-</xsl:text>
          <xsl:number format="1" level="any" count="src:activity"/>
          <xsl:text>');</xsl:text>
        </xsl:attribute>
        <xsl:choose>
          <xsl:when test="@status='active'">
            <xsl:attribute name="class"><xsl:text>active</xsl:text></xsl:attribute>
          </xsl:when>
          <xsl:when test="@status='abandoned'">
            <xsl:attribute name="class"><xsl:text>abandoned</xsl:text></xsl:attribute>
          </xsl:when>
          <xsl:when test="@status='complete'">
            <xsl:attribute name="class"><xsl:text>complete</xsl:text></xsl:attribute>
            <xsl:if test="@completed">
              <xsl:value-of select="@completed"/>
              <xsl:text>: </xsl:text>
            </xsl:if>
          </xsl:when>
        </xsl:choose>
        <xsl:apply-templates select="./src:title"/>
      </span>
      <xsl:if test="./src:description">
        <xsl:text>&#x00A0;*</xsl:text>
      </xsl:if>
      <xsl:if test="./src:sub-activities/src:activity">
        <ul>
          <xsl:apply-templates select="./src:sub-activities/src:activity"/>
        </ul>
      </xsl:if>
    </li>
  </xsl:template>

  <xsl:template match="//src:activity/src:title//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="//src:activity/src:title//src:a">
    <a href="{@href}"><xsl:apply-templates/></a>
  </xsl:template>

  <xsl:template match="//src:activity/src:title//src:a//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="src:description">
    <p>
      <xsl:attribute name="class">
        <xsl:text>term-</xsl:text>
        <xsl:number format="1" level="any" count="src:activity"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="//src:activity/src:description//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="//src:activity/src:description//src:a">
    <a href="{@href}"><xsl:apply-templates/></a>
  </xsl:template>

  <xsl:template match="//src:activity/src:description//src:a//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>
